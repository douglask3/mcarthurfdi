##########################################################################################
## Configure																			##
##########################################################################################
combineFDIs_cfg <- function () {
	source("cfg.r")
	cfg()
}

combineFDIs <- function() {
    combineFDIs_cfg();fname=match.call.string()
    lapply(filenameOut,combineFDImodel,fname)
}

combineFDImodel <- function(filenamOut,fname) {
		
	files=findOutFiles(filenamOut)
	dat=openCombineDat()
	apply(files,1,combineFDI,dat=dat,fname=fname)
}

combineFDI <- function(files,...) {
	yrs=findYrs(files)
	lapply(yrs,combineFDI4Yr,files,...)
}

combineFDI4Yr <- function(yr,files,...) {
	files=lapply(files,greplReturnXind,yr)
	nfiles=sapply(files,length)
	if (max(nfiles)==1) combineAndWrite(files,...)  else 
		for (i in 1:min(nfiles)) combineAndWrite(list(files[[1]][i],files[[2]][i]),...)	
}

combineAndWrite <- function(files,...) {
	c(CFDI,fname,vid):=combineFUN(files,...)
	writeCombinedFDI(CFDI,files[[1]],names(raster(files[[1]])),fname,vid)
}

writeCombinedFDI <- function(dat,file,vname,funName,vid) {
	fname=addCFDI(file,vid);vname=addCFDI(vname,vid)
	if (nlayers(dat)==1 ) writeRasterStandard(dat,NaN,fname,vname,funName,fileExt=FALSE)
	if (nlayers(dat)==12) writeClimRaster(dat,NaN,fname,vname,funName,fileExt=FALSE)
	if (nlayers(dat)==5 ) writeRasterStandard(dat,NaN,fname,vname,funName,fileExt=FALSE,
											  zname='quantile')
}

addCFDI <- function(x,vid='cfdi') {
	fname=splitstr(x,names(FDIfuns)[1])
	if (length(fname)==1) fname[2]=""
	return(paste(fname[1],vid,fname[2],sep=""))
}

findYrs <- function(files) {
	findYrss <- function(files) {
		yrs=tail(unique(splitstr(files,"-")),1)
		yrs=as.numeric(splitstr(yrs,".",fixed=TRUE)[1,])
		return(yrs)
	}
	yrss=lapply(files,findYrss)
	
	
	yrs=yrss[[1]]
	for (i in 1:length(yrss)) yrs=intersect(yrs,yrss[[i]])
	return(yrs)
}



##########################################################################################
## Sort files																			##
##########################################################################################

findOutFiles <- function(filenameOut) {
	files=list.files(out_dir,full.name=T)
	files=files[grepl(filenameOut,files)]
	files=lapply(paste(names(FDIfuns),"-",sep=""),splitIntoOuts,files)
	files=matrix(unlist(files,recursive=FALSE),ncol=length(FDIfuns))
    return(files)
}

splitIntoOuts <- function (fdiID,files) {
	fdiFiles=files[grepl(fdiID,files)]
	outputID=unique(splitstr(fdiFiles,'-')[1,])
	#commonStr=commonString(outputID)
	#outputID=splitstr(outputID,commonStr)[2,] files[grepl(filenameOut,files)]

	fdiFiles=lapply(outputID,greplReturn,fdiFiles)
	return(fdiFiles)
}



openCombineDat <- function(dat) {
	brickRev <- function(varname,filename) brick(filename,varname=varname)
	dat=layer.apply(combineDatVarn,brickRev,combineDatFile)
	sdat=sum(dat)
	dat[[nlayers(dat)]][sdat==0]=1
	dat=dat/sdat
	return(dat)
}
