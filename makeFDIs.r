##########################################################################################
## Configure																			##
##########################################################################################
makeFDIs_cfg <- function () {
	
	source("cfg.r")
	cfg()
	
	ml <<- monthLength()
}

makeFDIs <- function() {
	makeFDIs_cfg();fname=match.call.string()
	#mapply(makeFDIsGubbins,climPaths,filenameOut,fname)
	mListApply(makeFDIsGubbins,climPaths,filenameOut,fname)
}
##########################################################################################
## Link initialisations, daily weather generation, daily FDI, annual outputs and saved	##
## end conditions together																##
##########################################################################################
makeFDIsGubbins <- function(climPaths,filenameOut,fname=match.call.string()) {
	
	################
	## Initialize ##
	################
	c(clims,aPr,aWind,lat):=openClimate(climPaths)
    c(blankRasts,nDry,kb,dpr,clims,dd,yr,startp,nIterations):=initialization(clims)
    c(FDIout,nn):=annualInitialize(blankRasts,yr,length(FDIfuns))
	
	################
	## Time loops ##
	################
	for (m in 1:(nlayers(clims[[1]])-1)) {
		#######################################################
		## Find daily weather from monthly climate variables ##
		#######################################################
		c(mnth,mLength):=monthlyInitialize(m)
		c(dtemp,dtMax,dtMin,dclt,dWind,dpr):=dailyClims(clims,dpr,aWind[[yr+1]],mLength,m)
		
		##########################################
		## Perform daily FDI and add to outputs ##
		##########################################
		for (d in 1:ml[mnth]) {
			dd=dailyInitialize(dd,nIterations)
           			nDry=findNdry(nDry,dpr[[d]])
			
			c(FDI,kb):=ret.mult.apply(FDIfuns,function(i)
				i(dtemp[[d]],dtMax[[d]],dtMin[[d]],dclt[[d]],dWind[[d]],dpr[[d]],
				  aPr[[yr+1]],nDry,kb[[1]],d,lat))
			
			if (yr>=startp) FDIout=mapply(calAnnualStats,FDIout,FDI,mnth,mLength,
										  FDIdangerQuantiles,SIMPLIFY=FALSE)
		}
		#######################################
		## Annual output and initialisations ##
		#######################################
		if (mnth==12) {
			if (yr>=startp)
				mapply(outputAnnual,FDIout,filenameOut,yr+syrOut,fname,names(FDIfuns))
			
			c(FDIout,yr):=annualInitialize(blankRasts,yr,length(FDIfuns))
		}
	}
	
	######################
	## Output end state ##
	######################
    if (new2Initilize) outputEndState(c(dpr=dpr,nDry=nDry,kb=kb[[1]],clims))
}

##########################################################################################
## Initial Calls																		##
##########################################################################################
initialization <- function(clims) {
	nIterations=nlayers(clims[[1]])*365/12
	
	c(blankRast,mblankRast,qblankRast):=initializeBlankOuts(clims[[1]][[1]])
	
    InitialVals=ReadInEndState(blankRast,c('dpr','nDry','kb',names(clims)))
    c(dpr,nDry,kb,clims0):=InitialVals
    
    if (sum(sapply(InitialVals,filename)=="")==0) {
      	clims=mapply(addLayerMemSafe,b=clims,a=clims0,2,addName='0IsPrevious')
        startp=1
        dd=0
    } else {
    	clims=lapply(clims,addLayerMemSafe,a=blankRast,fnameID=2,addName='0IsBlank',overwite=TRUE)
        startp=2
        dpr=PrWetherGeneratorRaster(clims$wetdays[[1]],clims$pr[[1]],NULL,ml[1])
        dd=ml[1]
    }
	yr=0
	loopPCinitiate(0.01)
    
	return(list(list(blankRast,mblankRast,qblankRast),
				nDry,kb,dpr,(clims),dd,yr,startp,nIterations))
}

##########################################################################################
## Annual Calls																			##
##########################################################################################
annualInitialize <- function(blankRasts,yr,l) {
	yr=yr+1
	
	## Initialize output field
	index=c(aFDI=1,maxFDI=1,mFDI=2,qFDI=3)
	FDIout=blankRasts[index]
	names(FDIout)=names(index)
	
	FDIout=repRaster(FDIout,l)
	return(list(FDIout,yr))
}

calAnnualStats <- function(FDIout,FDI,mnth,mLength,FDIdangerQuantiles) {
	
	FDIout$aFDI=FDIout$aFDI+FDI/365
	FDIout$mFDI[[mnth]]=FDIout$mFDI[[mnth]]+FDI/mLength
	
	FDIout$maxFDI=reFindMax(FDIout$maxFDI,FDI)
	
	FDIout$qFDI=splitInQuantiles(FDI,out=FDIout$qFDI,lims=FDIdangerQuantiles)
	
	return(FDIout)
}	

##########################################################################################
## Monthly Calls																		##
##########################################################################################
monthlyInitialize <- function(m) {
	mnth=m-12*floor((m-2)/12)-1
	
	mLength=ml[mnth]
	cat("\nmonth:\t");cat(m);cat("\n")
	
	return(list(mnth,mLength))
}

##########################################################################################
## Daily Calls																			##
##########################################################################################
dailyInitialize <- function(dd,nIterations) {
	loopPC(nIterations)
	return(calDayOfYr(dd))
}

dailyClims <- function(clims,dpr,aWind,mLength,m) {
	dpr=PrWetherGeneratorRaster(clims$wetdays[[m]],clims$pr[[m]],dpr[[nlayers(dpr)]],mLength)
    dWind=UvasWetherGeneratorRaster(clims$wind[[m]],aWind,mLength)
    return(c(findDaily(clims,mLength,m),dWind,dpr))
}

findDaily <- function(clims,ml,m)
    lapply(clims[c('temp','tempMax','tempMin','Cloud')],
		  rasterInterp,ml,m:(m+1))

findNdry <- function(nDry,dpr) {
	testDry=dpr==0

	nDry[testDry]=nDry[testDry]+1
	nDry[!testDry]=0
	return(nDry)
}
