ret.mult.apply <- function(index,FUN,...) {
	
	classi=class(index)
	if (classi=="Raster" || classi=="RasterStack" || classi=="RasterBrick") {
		x=list()
		for (i in 1:length(index)) x[[i]]=match.fun(FUN)(index[[i]],...)
	} else {
		x=lapply(index,match.fun(FUN),...)
	}
	
	b=vector("list",length(x))
	a=list()
	
	for (i in 1:length(x[[1]])) a[[i]]=b
	for (i in 1:length(x)) for (j in 1:length(x[[i]])) a[[j]][[i]]=x[[i]][[j]]
	
	return(a)
}

ret.mult.mat.apply <- function(index,FUN,...) {
	a=ret.mult.apply(index,FUN,...)
	return(lapply(a,try.for.matrix))
}

try.for.matrix <- function(a) {
	if(sum(diff(sapply(a,length))!=0)!=0) return(a)
	
	b=a[[1]]
	for (i in a[-1]) b=cbind(b,i)
	colnames(b)=names(a)
	return(b)
}