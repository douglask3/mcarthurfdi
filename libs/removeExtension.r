removeExtension <- function(chars,ext='.gz') {	
	if (is.null(ext)) ext=paste('.',tail(strsplit(chars,'.',fixed=TRUE)[[1]],1),sep='')
	
	nc=nchar(ext)
	if (grepl(ext,chars)) chars=substr(chars,1,nchar(chars)-nc)
	return(chars)
}