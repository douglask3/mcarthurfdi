writeRasterStandard <- function(dat,yr,filename,varname,fname=match.call.string(),
								wname=match.call.string(),fileExt=TRUE,...) {
	
	if (fileExt)
        filename=paste(filename,varname,"-FDI_",return_git_version_number(),'-',yr,'.nc',sep="")
	writeRaster(dat,filename,varname=varname,overwrite=TRUE,xname='lon',yname='lat',...)
	
	nc=open.ncdf(filename,write=TRUE)
		att.put.ncdf(nc,0,"gitRepositoryURL",return_git_remoteURL())
		att.put.ncdf(nc,0,"gitRevisionNumber",return_git_version_number())
		att.put.ncdf(nc,0,"MainFunctionName",fname)
		att.put.ncdf(nc,0,"writeFunctionName",wname)
	close.ncdf(nc)
}

writeClimRaster <- function(...)
	writeRasterStandard(wname=match.call.string(),zname='time',zunit='month',...)


writeTempRaster <- function(dat,...) writeRaster(dat,filenameTemp(...),overwrite=TRUE)

filenameTemp <- function(name,no) paste(tempFileDir,name,no,sep="")