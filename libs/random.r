random <- function(reSet=FALSE) {
	intializeRandomNos(reSet)
	
	randNoN <<- randNoN+1
	return(randNos[randNoN,1])
}

intializeRandomNos <- function(reSet) {
	nNos = 100000
	filename='data/randomNo.csv'
	if (!file.exists(filename)) {
		dat=sample(seq(0,1,length.out=nNos),nNos,replace=TRUE)
		write.csv(dat,filename,row.names=FALSE)
	}
	if (!exists('randNos') || reSet || randNoN>=nNos) {
		randNos <<- read.csv(filename)
		randNoN	<<- 0
		nNos=length(randNos)
	}
}