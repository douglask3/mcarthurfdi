addLayerMemSafe <- function(a,b,fnameID=1,addName=NULL,overwrite=FALSE,...) {
	filename=filename(list(a,b)[[fnameID]])
	if (is.null(addName)){
		return(addLayerMemSafeGubbins(a,b,filename,overwrite=TRUE,...))
	} else {
		filename=paste(removeExtension(filename,NULL),addName,'.grd',sep='')
		if (!overwrite && file.exists(filename)) return(brick(filename))
		return(addLayerMemSafeGubbins(a,b,filename,overwrite=TRUE,...))
	}
}
	
	
addLayerMemSafeGubbins <- function(a,b,filename,...)
	writeRaster(addLayer(a,b),filename,...)