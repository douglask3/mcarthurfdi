ret.multi.mapply <- function(...) {
	a=mapply(...)
	b=list()
	for (i in 1:(dim(a)[1])) b[[i]]=a[i,]
	return(b)
}