mListApply <- function(FUN,testArg,...)
	if(class(testArg)=='list') return(mapply(FUN,testArg,...)) else
							   return(FUN(testArg,...))