rasterInterp <- function(dat,nd,ID=1:2) layer.apply(1:nd,rasterMidInterp,dat[[ID]],nd)

rasterMidInterp <- function(i,dat,...) {
	c(a,b):=distFind(i,...)
	return(dat[[1]]*a+dat[[2]]*b)
}

distFind <- function(i,nd) {
 	nd=nd-1
 	a=(i-1)/nd
 	b=1-a
 	return(list(a,b))
}