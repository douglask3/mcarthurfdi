match.call.string <- function(call=sys.call(sys.parent(n = 1)),...) {
	test=try(as.character(match.call(call=call,...))[1],silent=TRUE)
	if (class(test)=="try-error") return(as.character(call)[1]) else return(test)
}