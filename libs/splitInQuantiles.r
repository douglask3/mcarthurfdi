splitInQuantiles <- function(r,lims=c(5,13,25,50),out=NULL) {
	nlims=length(lims)+1
	if(is.null(out)) out=BrickFromRaster(r,nlims,!is.na(r))
	
	lims=c(-9E9,lims,9E9)
	for (i in 1:nlims) {
		test=r>lims[i] & r<= lims[i+1]
		out[[i]][test]=out[[i]][test]+1
	}
	return(out)
}