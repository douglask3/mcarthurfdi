##########################################################################################
## Parameters																			##
##########################################################################################
Futr_Paths <- function() {
	
	modPath		<- '~/Documents/climInputs/climData/RCP_Futures/processed/'
	climPaths 	<- list.files(modPath,full.names=TRUE)
	modNames	<- list.files(modPath)
	RCP			<- '4.5'
	
	filenameOut <<- paste("outputs/RCP",RCP,"_",modNames,"_CO2_nolightn","-",sep="")
	
	ClimFileIDs <- c(
		temp	=	'tas_',
		tempMax	=	'tasmax_',
		tempMin	=	'tasmin_',
		Cloud	=	'clt_',
		pr		=	'pr_',
		wetdays	= 	'wetdays_',
		wind	=	'uvas_')
		
	syrOut		<<- 10156
	
	return(list(climPaths,lapply(climPaths,list.filesIDed,ClimFileIDs,RCP)))
}

list.filesIDed <- function(path,IDs,RCP,...) {
	files=list.files(path,...)
	return(sapply(IDs,function(i) files[grepl(i,files) & grepl(RCP,files)]))
}

Hist_Paths <- function() {
	climPaths 	<- '~/Documents/climInputs/climData/historic/'
	
	climFiles	<- c(
		temp	=	'tas_18512006.nc',
		tempMax	=	'tasmax_18512006.nc',
		tempMin	=	'tasmin_18512006.nc',
		Cloud	=	'clt_18512006.nc',
		pr		=	'pr_18512006.nc',
		wetdays	= 	'wetdays_18512006.nc',
		wind	=	'uvas_18512006.nc')
		
	filenameOut <<- 'outputs/historic_CRU_TS3.1_'
	syrOut		<<- 10000
	return(list(climPaths,list(climFiles)))
}

Paths <- function() {
	makeGlobDir("tempFileDir",'temp/')
		
    filenameEnd <<- 'outputs/EndState_'
	
	return(Hist_Paths())
}
climScaler <- function() 
	climScale <<- list(
		temp	= c('-',273.15),
		tempMax	= c('-',273.15),
		tempMin	= c('-',273.15),
		Cloud	= NULL,
		pr		= NULL,#c('*',60*60*24*365/12),#NULL,
		wetdays	= NULL,
		Wind	= NULL)


locationInfo <- function() {
	lon_range	<<- c(112.5,154)
	lat_range 	<<- c(-44,-10.75)
}

FDIfunctions <- function() {
	FDIfuns				<<- list('ffdi'=FFDI,'gfdi'=GFDI)
	FDIdangerQuantiles	<<- list('ffdi'=c(5,12,24,50),'gfdi'=c(3,8,20,50))
	combine				<<- TRUE
	combineFUN			<<- combineFFDI_GFDI_step
	combineDatFile		<<- '../mcarthurfdiModern/data/veg_cont_fields_CRU.nc'
	combineDatVarn		<<- c('Tree_cover','Herb')
}

initialState <- function () {
    new2Initilize       <<- FALSE
    initializeFromOld   <<- TRUE
}

##########################################################################################
## configure parameters etc																##
##########################################################################################
cfg <- function() {
	source("libs/source_all_libs.r")
	
	source_all_libs()
	source_all_libs("io")
	source_all_libs("fdis")
    setup_project_structure()
	
	installRaster()
	include_libraries()
	calClimPaths()
	locationInfo()
	climScaler()
	FDIfunctions()
    initialState()
}

##########################################################################################
## functions for configuring															##
##########################################################################################

include_libraries <- function() {
	libraries <- c('raster','ncdf','parallel')
	sapply(libraries,function(i) install_and_source_library(i))
}
	
calClimPaths <- function() {
	paths=Paths()
	
	FindclimPaths <- function(paths1,paths2,...) {
		climPaths=paste(paths1,paths2,sep="/")
		names(climPaths)=names(paths2)
		return(climPaths)
	}
    
    climPaths <<- mapply(FindclimPaths,paths[[1]],paths[[2]],SIMPLIFY=FALSE)
	
	return(climPaths)
}

installRaster <- function() {
	install_and_source_library("raster")
	if(!exists('raster.resample')) raster.resample <<- resample
	if(!exists('raster.extend')) raster.extend <<- extend
	if(!exists('raster.area')) raster.area <<- area
}

