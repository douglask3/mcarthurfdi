outputAnnual <- function(FDIout, filenameOut,yr,fname=match.call.string(),fdiName) {
	makeVarname <- function(l) paste(l,fdiName,sep='')
	
	writeRasterStandard(FDIout$aFDI,yr,filenameOut,makeVarname('a'),fname)
	writeRasterStandard(FDIout$maxFDI,yr,filenameOut,makeVarname('max_'),fname)
	writeClimRaster(FDIout$mFDI,yr,filenameOut,makeVarname('m'),fname)
	writeRasterStandard(FDIout$qFDI,yr,filenameOut,makeVarname('quantile_'),fname,zname='quantile')
}