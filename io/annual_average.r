annual_average <- function(dat) {
	nyrs=nlayers(dat)/12
	
	filenameOut=paste(removeExtension(filename(dat),'.grd'),"AA.grd",sep="")

	if (file.exists(filenameOut)) return(brick(filenameOut))
	
	annualAverage <- function(i) {
		aa=sum(dat[[(1+(i-1)*12):(i*12)]])
		return(writeTempRaster(aa,tempFname(filenameOut,"sub"),i))
	}
	aa=layer.apply(1:nyrs,annualAverage)
	aa=addLayer(aa[[1]],aa)
	return(writeRaster(aa,filenameOut))
}