outputEndState <- function (outs) {
    outs=lapply(outs,function(i) i[[nlayers(i)]])
    mapply(writeRasterStandard,outs,0,filenameEndState(names(outs)),'EndState',match.call.string(),fileExt=FALSE)
}

filenameEndState <- function(names) paste(filenameEnd,names,".nc",sep="")

ReadInEndState <- function(blankRast,names) {
    readFile <- function(filename) {
        if (initializeFromOld & file.exists(filename)) return(raster(filename)) else return(blankRast)
    }
    
    return(lapply(filenameEndState(names),readFile))
}