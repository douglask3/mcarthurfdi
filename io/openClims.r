openClims <- function(path,Scale) {
	pathOut=tail(strsplit(path,'/')[[1]],1)
	
	pathOut=removeExtension(pathOut)
	pathOut=removeExtension(pathOut,'.nc')
	
	pathOut=paste(filenameTemp(pathOut,NULL),'.grd',sep='')
	if (file.exists(pathOut)) return(brick(pathOut))
	
	dat=brick.gunzip(path,lon_range=lon_range,lat_range=lat_range)
	
	nr <<- 1
	dat0=dat
	dat=layer.apply(1:ceiling(nlayers(dat)/72),removeMaskAndScale,dat,Scale,tempFname(pathOut))
	
	return(writeRaster(dat,pathOut,overwrite=TRUE))
}

removeMaskAndScale <- function(i,dat,Scale,fname) {
	index=(1+(i-1)*72):(i*72)
	index=index[index<=nlayers(dat)]
	dat=dat[[index]]
	
	if(!exists("nr")) nr <<- 1 else nr <<- nr+1
	dat[dat==-999]=NaN
	if(!is.null(Scale)) dat=match.fun(Scale[1])(dat,as.numeric(Scale[2]))
	return(writeTempRaster(dat,fname,nr))
}

tempFname <- function(path,strg="maskRemove") {
	name=removeExtension(tail(strsplit(path,'/')[[1]],1),NULL)
	return(paste(name,strg,sep=""))
}
