openClimate <- function(climPaths) {
	clims=mapply(openClims,climPaths,climScale)
	clims$pr=clims$pr*60*60*24*365/12
	
	aPr=annual_average(clims$pr)
	aWind=annual_average(clims$wind)
	
	lat=findLat(clims[[1]][[1]])
	return(list(clims,aPr,aWind,lat))
}