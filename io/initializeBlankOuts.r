initializeBlankOuts <- function(eg){
	
	c(blankRast,mask):=initialBlank(eg)
	
	mblankRast=BrickFromRaster(blankRast,12,mask)
	qblankRast=BrickFromRaster(blankRast,5,mask)
	
	return(list(blankRast,mblankRast,qblankRast))
}

initialBlank<- function(blankRast) {
	mask=!is.na(blankRast)
	blankRast[mask]=0
	blankRast[!mask]=NaN
	return(list(blankRast,mask))
}

BrickFromRaster <- function(r,nl=12,mask,initialV=0,...)  {
	mr=brick(raster(r),nl=nl,...)
	mr[mask]=initialV
	return(mr)
}