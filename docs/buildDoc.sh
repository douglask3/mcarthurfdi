## Copy reqired hooks
hooksPath="gitHooks/"
hooksDir="../.git/hooks/"

fnames=`ls $hooksPath`
cp `find $hooksPath | tail -n +2` $hooksDir

for i in `ls $hooksPath`
do
    f=$hooksDir$i
    echo $f
    chmod +x $f
done                                                                                                                                                                                                                                           

rm Info.bbl
rm Info.aux       
rm Info.blg     
rm Info.log
rm Info.pdf
rm Info.toc
              
pdflatex -interaction nonstopmode Info.tex
bibtex Info             
pdflatex -interaction nonstopmode Info.tex
pdflatex -interaction nonstopmode Info.tex
