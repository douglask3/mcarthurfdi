curing <- function(Tmax,Tmin,T,clt,aPr,d,lat) {
	H=Hr(Tmax,Tmin,T,clt,aPr,d,lat)
	FMC=5.658+0.4651*H+(3.151*H^3)/(T*10000)-0.1854*T^0.77
	cindex=109.6758-1.0721*H+0.0044*FMC^2-(6.295*H^3)/1000000
	cindex[cindex>100]=100
	cindex[cindex<0]=0
	return(cindex)
}