PrWetherGenerator <- function(wd,pr,dpr0,mLength) {
	
	c1=1.0; c2=1.2
	
	if (wd<1) wd=1
	if (is.null(dpr0) || is.na(dpr0)) dpr0=0
	
	dval_prec=rep(0,mLength)
	if (pr==0) return(dval_prec)
	
	prob_rain=wd/mLength
	dpr=pr/wd
	for (day in 1:mLength) {
		# Transitional probabilities (Geng et al. 1986)
		if(day==1) test=dpr0 else test=dval_prec[day-1]
		if(test<0.1) prob=0.75*prob_rain else prob=0.25+(0.75*prob_rain)
		vv=random()
		
		# Determine wet days randomly and use Krysanova/Cramer estimates of
		# parameter values (c1,c2) for an exponential distribution
		if(vv>prob) dval_prec[day]=0 else {
			v1=random()
			dval_prec[day]=((-log(v1))^c2)*dpr*c1
			if(dval_prec[day]<0.1) dval_prec[day]=0.0
		}
		
	}
	
	#normalise generated precipitation by monthly CRU values
	mpr=sum(dval_prec)
	if (mpr<1) mpr=1
	dval_prec=dval_prec*pr/mpr
	dval_prec[dval_prec<0.1]=0
	
	return(dval_prec)
}

PrWetherGeneratorRaster <- function(wd,pr,dpr0,mLength) {
	out0=wd
	
	wd=values(wd)
	pr=values(pr)
	
	test=!(is.na(wd+pr))
	
	wd=wd[test]
	pr=pr[test]
	if (is.null(dpr0)) dpr0=rep(0,length(wd)) else dpr0=dpr0[test]
	
	dpr=mapply(PrWetherGenerator,wd,pr,dpr0,MoreArgs=list(mLength=mLength))
	
	out0[]=NaN
	
	for (i in 1:dim(dpr)[1]) {
		out1=out0
		out1[test]=dpr[i,]
		if (i==1) out=out1 else out=addLayer(out,out1)
	}
	
	return(out)
}