DF <- function(T,Pr,aPr,nDry,kb) {
	nDryfact=(nDry+1)^1.5
	kb=kb+dKBDI(T,aPr,kb-Pr)
	return(list(kb,(0.191*(kb+104)*nDryfact)/(3.52*nDryfact+Pr-1)))
}

dKBDI <- function(T,aPr,kb)
	((203.2-kb)*(0.968*exp(0.0875*T+1.5552)-8.30)/(1+10.88*exp(-0.001736*aPr)))/1000
