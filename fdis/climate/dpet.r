dpet <- function(T,clt,day,lat) {
	k1=610.78;k2=17.269;k3=237.3; k=13750.98708
	a=107.0;b=0.2; C=0.25;d=0.5
	Beta=0.17; qoo=1360.0
	degtorad=2*pi/360
	
	lat=lat*degtorad

	lambda =2.495e6-T*2380.0
	Gamma=65.05+T*0.064
	
	sat=k1*k2*k3*exp(k2*T/(k3+T))/((k3+T)**2)
	ni=(100-clt)/100.0
	delta=-23.4*degtorad*cos(degtorad*360*(day+10.0)/365.0)
	rl=(b+(1.0-b)*ni)*(a-T)
	qo=qoo*(1.0+2.0*0.01675*cos(2.0*pi*day/365.0))  
	
	w=(C+d*ni)*(1.0-Beta)*qo
	
	uu=w*sin(lat)*sin(delta)-rl
	vv=w*cos(lat)*cos(delta)
	hn=HN(uu,vv)
	
	return(1.26*2.0*(sat/(sat+Gamma)/lambda)*(uu*hn+vv*sin(hn))*k)
}
HN <- function(uu,vv) {
	test1=uu>vv
	test2=uu<(0-vv)
	
	hn=acos((0-uu)/vv)
	hn[test1]=pi
	hn[test2]=0
	return(hn)
}