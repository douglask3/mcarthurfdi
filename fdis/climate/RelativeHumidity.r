Hr <- function(Tmax,Tmin,T,clt,aPr,d,lat) {
	Tdews=Tdew(Tmax,Tmin,T,clt,aPr,d,lat)
	return(100*exp((17.271*Tdews)/(237.7+Tdews))/exp((17.271*Tmax)/(237.7+Tmax)))
}

Tdew <- function(Tmax,Tmin,T,clt,aPr,d,lat) {
	EFs=dpet(T,clt,d,lat)/aPr
	WEF=1.003-1.444*EFs+12.312*EFs^2-32.766*EFs^3
	return(-273.15+(Tmin+273.15)*(-0.127+1.121*WEF+0.0006*(Tmax-Tmin)))
}