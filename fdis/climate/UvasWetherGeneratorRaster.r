UvasWetherGeneratorRaster <- function(mdat,adat,nd) {
	mdat[mdat<0]=0
    cv=12/adat
	sj=cv*mdat
	gammaj=mdat/(sj^2)
	sigmaj=(mdat^2)/(sj^2)
	
	CDF=layer.apply(1:nd, function(i) pgammaSelectRaster(mdat,sigmaj,gammaj))
	CDF=CDF*mdat/mean(CDF)
	return(CDF)
}

pgammaSelectRaster <- function(mn,s1,s2) {
	out=mn
	values(out)=mapply(pgammaSelect,values(mn),values(s1),values(s2))
	return(out)
}

pgammaSelect <- function(m,i,j) {
	if (is.na(m) || m==0) return(m)
	if (is.na(i) || is.na(j)) return(i)
	x=seq(1,m*20,length.out=200)
	v1=random()
	cdf=1-pgamma(x,i,j)
	return(x[which.min(abs(cdf-v1))])
}
