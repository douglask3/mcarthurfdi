combineFFDI_GFDI_linear <- function(files,dat,fname) {
	FFDI=brick(files[[1]])
	GFDI=brick(files[[2]])
	
	layerCombine <- function(i) FFDI[[i]]*dat[[1]]+GFDI[[i]]*dat[[2]]
	CFDI=layer.apply(1:nlayers(FFDI),layerCombine)
	
	fname=paste(fname,'combineFFDI_GFDI_linear',sep="/")
	return(list(CFDI,fname,'cfdi'))
}

combineFFDI_GFDI_step <- function(files,dat,fname) {
	FFDI=brick(files[[1]])
	GFDI=brick(files[[2]])
	
	dat[[2]]=dat[[1]]<0.01
	dat[[1]]=dat[[1]]>0.01
	
	layerCombine <- function(i) FFDI[[i]]*dat[[1]]+GFDI[[i]]*dat[[2]]
	CFDI=layer.apply(1:nlayers(FFDI),layerCombine)
	
	fname=paste(fname,'combineFFDI_GFDI_step',sep="/")
	return(list(CFDI,fname,'sfdi'))
}

